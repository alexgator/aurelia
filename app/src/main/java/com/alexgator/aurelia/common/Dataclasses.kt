package com.alexgator.aurelia.common

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Created by AlexGator on 15.03.18.
 */

/* View-Presenter */
data class EntryListItem(
        val id: Long,
        val title: String,
        val description: String,
        val date: String,
        val url: String,
        val enclosureUrl: String
)

/* Presenter-Model */
data class Entry(
        val id: String,
        val title: String,
        val description: String,
        val date: Long,
        val url: String,
        val enclosureUrl: String
)

data class Article(
        val title: String,
        val body: String,
        val imageUrl: String,
        val date: Long,
        val url: String
)

/* Serializable classes to parse incoming JSON using kotlinx.serialization package*/
@Serializable
data class Data(val root: Root)

@Serializable
data class Root(
        val url: String,
        @Optional
        @SerialName("share_image")
        val shareImage: String = "",
        @SerialName("published_at")
        val publishedAt: Long,
        val title: String,
        val content: Content
)

@Serializable
data class Content(
        @Optional
        val body: String = ""
)

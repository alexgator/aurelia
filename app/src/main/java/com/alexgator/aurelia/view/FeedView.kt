package com.alexgator.aurelia.view

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.alexgator.aurelia.R
import com.alexgator.aurelia.app
import com.alexgator.aurelia.common.EntryListItem
import com.alexgator.aurelia.di.FeedComponent
import com.alexgator.aurelia.di.FeedModule
import com.alexgator.aurelia.presenter.FeedPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.feed_activity.*
import kotlinx.android.synthetic.main.feed_list_item.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.support.v4.onRefresh
import javax.inject.Inject

interface FeedView {
    val component: FeedComponent

    fun showFeedItems(data: List<EntryListItem>)
    fun navigateToEntryView(url: String)
    fun setRefreshing(refreshing: Boolean)
    fun showNonCriticalException(text: String)
}

class FeedActivity : AppCompatActivity(), FeedView {

    override val component by lazy { app.component.plus(FeedModule(this)) }

    @Inject
    lateinit var presenter: FeedPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feed_activity)
        setSupportActionBar(toolbar)

        component.inject(this)

        feedRecycler.layoutManager = LinearLayoutManager(this)

        presenter.onCreate()

        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.onRefresh {
            presenter.updateEntries()
        }
    }

    override fun showFeedItems(data: List<EntryListItem>) {
        feedRecycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick),
                false)
    }

    private fun onRecyclerItemClick(item: EntryListItem) {
        presenter.onItemSelected(item)
    }

    override fun setRefreshing(refreshing: Boolean) {
        swipeRefresh.isRefreshing = refreshing
    }

    override fun navigateToEntryView(url: String) {
        startActivity(intentFor<EntryActivity>(
                EntryActivity.EXTRA_URL to url
        ))
    }

    override fun showNonCriticalException(text: String) {
         Snackbar.make(layout, text, Snackbar.LENGTH_LONG)
                 .apply { show() }
    }

    private class RecyclerAdapter(
            private val data: List<EntryListItem>,
            private val listener: (EntryListItem) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init {
            setHasStableIds(true)
        }

        override fun getItemId(position: Int): Long = data[position].id

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.feed_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(data[position], listener)
        }

        class ViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
            val description: TextView   = view.description
            val date: TextView          = view.date
            val picture: ImageView      = view.contentImage

            fun bind(item: EntryListItem, listener: (EntryListItem) -> Unit) {
                description.text = item.description
                date.text = item.date

                Picasso.get()
                        .load(item.enclosureUrl)
                        .placeholder(R.drawable.meduza)
                        .into(picture)

                view.setOnClickListener { listener(item) }
            }
        }
    }
}

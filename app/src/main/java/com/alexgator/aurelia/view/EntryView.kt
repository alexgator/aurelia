package com.alexgator.aurelia.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.alexgator.aurelia.R
import com.alexgator.aurelia.app
import com.alexgator.aurelia.di.EntryComponent
import com.alexgator.aurelia.di.EntryModule
import com.alexgator.aurelia.presenter.EntryPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.entry_activity.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

interface EntryView {
    val component: EntryComponent

    fun showTitle(text: String)
    fun showText(text: String)
    fun showImage(url: String)
    fun showDate(text: String)
    fun openUrl(articleUrl: String)
    /**
     * Show exception and finish activity
     * */
    fun showCriticalException(string: String)
}

class EntryActivity : AppCompatActivity(), EntryView {

    companion object {
        const val EXTRA_URL = "url"
    }

    override val component by lazy {
        app.component.plus(EntryModule(this))
    }

    @Inject
    lateinit var presenter: EntryPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.entry_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        component.inject(this)

        val url = intent.getStringExtra(EXTRA_URL)

        presenter.onCreate(url)

        addListeners()
    }

    private fun addListeners() {
        fab.onClick {
            presenter.onOpenFullVersionBtnClick()
        }
    }

    override fun showTitle(text: String) {
        contentTitle.text = text
    }

    override fun showText(text: String) {
        contentText.text = text
    }

    override fun showDate(text: String) {
        contentDate.text = text
    }

    override fun showImage(url: String) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.meduza)
                .into(contentImage)
    }

    override fun openUrl(articleUrl: String) {
        if (articleUrl == "") return
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(articleUrl)))
    }

    override fun showCriticalException(string: String) {
        longToast(string)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}

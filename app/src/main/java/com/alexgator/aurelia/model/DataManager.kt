package com.alexgator.aurelia.model

import android.util.Log
import com.alexgator.aurelia.App
import com.alexgator.aurelia.common.Article
import com.alexgator.aurelia.common.Entry
import kotlinx.coroutines.experimental.*
import javax.inject.Inject

/**
 * Created by AlexGator on 15.03.18.
 */

interface DataManager {
    /**
     * Get an article by [url] from API and cache it, or from DB the article is already cached
     * */
    suspend fun getArticle(url: String): Deferred<Article>
    /**
     * Get cached entries from database
     * */
    suspend fun getCachedEntries(): List<Entry>
    /**
     * Fetch entries from RSS and cache them
     * */
    suspend fun fetchEntries(): Deferred<List<Entry>>
}

class DataManagerImpl(app: App): DataManager {
    @Inject
    lateinit var rssHelper: RSSHelper

    @Inject
    lateinit var apiHelper: APIHelper

    @Inject
    lateinit var dbHelper: DBHelper

    init {
        app.component.inject(this)
    }

    override suspend fun getCachedEntries(): List<Entry> {
        return dbHelper.getFeedEntries()
    }

    override suspend fun fetchEntries(): Deferred<List<Entry>> {
        val def = CompletableDeferred<List<Entry>>()
        launch {
            try {
                val entries = rssHelper.fetchEntries()
                def.complete(entries)
                dbHelper.addEntriesIfNotExist(entries)
            } catch (e: Throwable) {
                def.cancel(e)
            }
        }
        return def
    }

    override suspend fun getArticle(url: String): Deferred<Article> {
        val def = CompletableDeferred<Article>()

        launch {
            try { // to get from DB
                val cached = dbHelper.getArticleByUrl(url)

                val article = if (cached == null) {
                    // DB don't have entry with the url, so fetch it from API and cache
                    val fetched = apiHelper.fetchArticle(url)
                    dbHelper.addArticleIfNotExist(fetched)
                    fetched
                } else {
                    // DB already have the entry, simple return it
                    cached
                }
                def.complete(article)
            } catch (e: Throwable) {
                def.cancel(e)
            }
        }

        return def
    }
}

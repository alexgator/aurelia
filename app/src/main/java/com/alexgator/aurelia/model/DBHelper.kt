package com.alexgator.aurelia.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.alexgator.aurelia.common.Article
import com.alexgator.aurelia.common.Entry
import org.jetbrains.anko.db.*

/**
 * Created by AlexGator on 15.03.18.
 */

interface DBHelper {
    /**
     * For each entry check if it already exits and insert otherwise
     * */
    fun addEntriesIfNotExist(entries: List<Entry>)
    /**
     * Check if the [article] already exist and insert it otherwise
     * */
    fun addArticleIfNotExist(article: Article)
    /**
     * Get cached entries
     * */
    fun getFeedEntries(): List<Entry>
    /**
     * Get cached article by url
     *
     * @return article if exist and null otherwise
     * */
    fun getArticleByUrl(url: String): Article?
}

class DBHelperImpl(context: Context)
    : ManagedSQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION), DBHelper {

    companion object {
        const val DATABASE_NAME     = "aurelia"
        const val DATABASE_VERSION  = 1

        const val TABLE_ENTRIES     = "entries"

        const val COL_ENTRY_ID      = "id"
        const val COL_ENTRY_TITLE   = "title"
        const val COL_ENTRY_DESCR   = "description"
        const val COL_ENTRY_DATE    = "date"
        const val COL_ENTRY_URL     = "url"
        const val COL_ENTRY_ENC_URL = "enclosureUrl"

        const val TABLE_ARTICLES = "articles"

        const val COL_ARTICLE_TITLE = "title"
        const val COL_ARTICLE_BODY  = "body"
        const val COL_ARTICLE_IMG   = "imageUrl"
        const val COL_ARTICLE_DATE  = "date"
        const val COL_ARTICLE_URL   = "url"

        private val entriesParser = rowParser {
            id: String, title: String, descr: String, date: Long, url: String, encUrl: String ->
            Entry(id, title, descr, date, url, encUrl)
        }

        private val articlesParser = rowParser {
            title: String, body: String, imageUrl: String, date: Long, url: String ->
            Article(title, body, imageUrl, date, url)
        }
    }

    override fun addEntriesIfNotExist(entries: List<Entry>) {
        val existed = getFeedEntries()
        val new = entries.subtract(existed)
        new.forEach {
            use {
                insert(TABLE_ENTRIES,
                        COL_ENTRY_ID        to it.id,
                        COL_ENTRY_TITLE     to it.title,
                        COL_ENTRY_DESCR     to it.description,
                        COL_ENTRY_DATE      to it.date,
                        COL_ENTRY_URL       to it.url,
                        COL_ENTRY_ENC_URL   to it.enclosureUrl
                )
            }
        }
    }

    override fun addArticleIfNotExist(article: Article) {
        if (getArticleByUrl(article.url) == null) {
            use {
                insert(TABLE_ARTICLES,
                        COL_ARTICLE_TITLE   to article.title,
                        COL_ARTICLE_BODY    to article.body,
                        COL_ARTICLE_IMG     to article.imageUrl,
                        COL_ARTICLE_DATE    to article.date,
                        COL_ARTICLE_URL     to article.url)
            }
        }
    }

    override fun getFeedEntries(): List<Entry> = use {
        select(TABLE_ENTRIES,
                COL_ENTRY_ID,
                COL_ENTRY_TITLE,
                COL_ENTRY_DESCR,
                COL_ENTRY_DATE,
                COL_ENTRY_URL,
                COL_ENTRY_ENC_URL
        ).parseList(entriesParser)
    }

    override fun getArticleByUrl(url: String): Article? {
        return try {
            use {
                select(TABLE_ARTICLES,
                        COL_ARTICLE_TITLE,
                        COL_ARTICLE_BODY,
                        COL_ARTICLE_IMG,
                        COL_ARTICLE_DATE,
                        COL_ARTICLE_URL
                )
                        .whereSimple("($COL_ARTICLE_URL = \"$url\")")
                        .parseOpt(articlesParser)
            }
        } catch (e: Exception) {
            Log.e("DBHELPER", "Error while trying to get article", e)
            null
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(TABLE_ENTRIES, true, "_id" to INTEGER + PRIMARY_KEY,
                COL_ENTRY_ID        to TEXT,
                COL_ENTRY_TITLE     to TEXT,
                COL_ENTRY_DESCR     to TEXT,
                COL_ENTRY_DATE      to INTEGER,
                COL_ENTRY_URL       to TEXT,
                COL_ENTRY_ENC_URL   to TEXT
        )

        db.createTable(TABLE_ARTICLES, true, "_id" to INTEGER + PRIMARY_KEY,
                COL_ARTICLE_TITLE   to TEXT,
                COL_ARTICLE_BODY    to TEXT,
                COL_ARTICLE_IMG     to TEXT,
                COL_ARTICLE_DATE    to INTEGER,
                COL_ARTICLE_URL     to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(TABLE_ENTRIES, true)
        db.dropTable(TABLE_ARTICLES, true)
        onCreate(db)
    }
}
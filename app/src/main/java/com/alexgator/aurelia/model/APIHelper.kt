package com.alexgator.aurelia.model

import com.alexgator.aurelia.common.Article
import com.alexgator.aurelia.common.Data
import kotlinx.serialization.json.JSON
import java.net.URL

/**
 * Created by AlexGator on 15.03.18.
 */
interface APIHelper {
    /**
     * Loads an article by provided [url]
     * */
    suspend fun fetchArticle(url: String): Article
}

class APIHelperImpl: APIHelper {
    companion object {
        const val BASE_URL = "https://meduza.io"
        const val API_URL = "$BASE_URL/api/v3/"
    }


    override suspend fun fetchArticle(url: String): Article {
        val json = URL(url.replace(BASE_URL, API_URL)).openStream().bufferedReader().readLine()
        val data = JSON.nonstrict.parse<Data>(json).root

        val preparedBody = data.content.body
                // Remove scripts
                .replace(Regex("<script\\b[^<]*(?:(?!</script>)<[^<]*)*</script>"), "")
                // HTML tags and escape sequences
                .replace(Regex("(<.*?>)|(&.*?;)"), "")
                // And redundant spaces
                .replace(Regex("(\\s){3,}"), "\n\n")
                .trim()

        return Article(
                data.title,
                preparedBody,
                "$BASE_URL/${data.shareImage}",
                data.publishedAt,
                "$BASE_URL/${data.url}"
        )
    }
}
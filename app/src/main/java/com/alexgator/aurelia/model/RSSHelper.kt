package com.alexgator.aurelia.model

import android.util.Xml
import com.alexgator.aurelia.common.Entry
import org.xmlpull.v1.XmlPullParser
import java.io.InputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by AlexGator on 14.03.18.
 */

interface RSSHelper {
    suspend fun fetchEntries(): List<Entry>
}


class RSSHelperImpl: RSSHelper {

    companion object {
        const val RSS_URL = "https://meduza.io/rss/all"

        const val TAG_ID = "guid"
        const val TAG_CHANNEL = "channel"
        const val TAG_ENTRY = "item"
        const val TAG_TITLE = "title"
        const val TAG_LINK = "link"
        const val TAG_DESCR = "description"
        const val TAG_ENCLOS = "enclosure"
        const val TAG_DATE = "pubDate"

        // Wed, 14 Mar 2018 15:22:58 +0300
        val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzzzz", Locale.US)
    }

    override suspend fun fetchEntries(): List<Entry> {
        return parse(URL(RSS_URL).openStream())
    }

    /**
     * Extract entries from XML
     * */
    private fun parse(inputStream: InputStream): List<Entry> {
        try {
            val parser = Xml.newPullParser()

            parser.apply {
                // We don't use namespaces
                setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                setInput(inputStream, null)
                //Skip <rss> and <channel> tags
                nextTag()
                nextTag()
            }

            return readRSS(parser)
        } finally {
            inputStream.close()
        }
    }

    private fun readRSS(parser: XmlPullParser): List<Entry> {
        val entries = mutableListOf<Entry>()
        // Make sure the parser is inside a channel tag
        parser.require(XmlPullParser.START_TAG, null, TAG_CHANNEL)
        // Process entries and skip other tags
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) continue

            if (parser.name == TAG_ENTRY) {
                entries.add(readEntry(parser))
            } else {
                skip(parser)
            }
        }

        return entries
    }

    private fun readEntry(parser: XmlPullParser): Entry {
        var id = ""
        var title = ""
        var description = ""
        var date = 0L
        var url = ""
        var enclosureUrl = ""

        parser.require(XmlPullParser.START_TAG, null, TAG_ENTRY)
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) continue
            when (parser.name) {
                TAG_ID -> id = readTextTag(parser, TAG_ID)
                TAG_TITLE -> title = readTextTag(parser, TAG_TITLE)
                TAG_DESCR -> description = readTextTag(parser, TAG_DESCR)
                TAG_DATE -> date = readDate(parser)
                TAG_LINK -> url = readTextTag(parser, TAG_LINK)
                TAG_ENCLOS -> enclosureUrl = readEnclosure(parser)
                else -> skip(parser)
            }
        }

        return Entry(id, title, description, date, url, enclosureUrl)
    }

    /**
     * Parse date string and return time in ms
     * */
    private fun readDate(parser: XmlPullParser): Long {
        return dateFormat.parse(readTextTag(parser, TAG_DATE)).time
    }

    /**
     * Read enclosure url attribute
     * */
    private fun readEnclosure(parser: XmlPullParser): String {
        parser.require(XmlPullParser.START_TAG, null, TAG_ENCLOS)
        val text = parser.getAttributeValue(null, "url")
        parser.next()
        return text
    }

    /**
     * Return string inside current tag
     */
    private fun readTextTag(parser: XmlPullParser, tag: String): String {
        var result = ""
        parser.require(XmlPullParser.START_TAG, null, tag)
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        parser.require(XmlPullParser.END_TAG, null, tag)
        return result
    }

    /**
     * Skip current tag
     * */
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }
}


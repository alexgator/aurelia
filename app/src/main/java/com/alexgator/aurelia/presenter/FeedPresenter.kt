package com.alexgator.aurelia.presenter

import android.content.Context
import com.alexgator.aurelia.R
import com.alexgator.aurelia.common.Entry
import com.alexgator.aurelia.common.EntryListItem
import com.alexgator.aurelia.model.DataManager
import com.alexgator.aurelia.view.FeedView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 15.03.18.
 */

interface FeedPresenter {
    fun onCreate()
    fun onItemSelected(item: EntryListItem)
    fun updateEntries()
}

class FeedPresenterImpl(private val view: FeedView): FeedPresenter {

    companion object {
        val dateFormat = SimpleDateFormat("dd MMM 'в' HH:mm", Locale("ru"))
    }

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    override fun onCreate() {
        view.component.inject(this)

        launch {
            val cached = convertEntriesToListItems(dataManager.getCachedEntries())
            launch(UI) { view.showFeedItems(cached) }
                    .join()

            updateEntries()
        }
    }

    /**
     * Fetch entries and update view
     * */
    override fun updateEntries() {
        launch {
            val idle = launch(UI) {
                // Show refreshing indicator after 1 sec to prevent indicator blinking
                delay(1000)
                view.setRefreshing(true)
            }
            try {
                val entries = convertEntriesToListItems(dataManager.fetchEntries().await())

                launch(UI) { view.showFeedItems(entries) }
                        .join()

            } catch (e: Exception) {
                launch(UI) {
                    view.showNonCriticalException(
                        appContext.getString(R.string.data_update_failed))
                }
            } finally {
                idle.cancel()
                launch(UI) { view.setRefreshing(false) }
            }
        }
    }

    private fun convertEntriesToListItems(list: List<Entry>): List<EntryListItem> = list.map {
        EntryListItem(stringHash31(it.id), it.title, it.description,
                dateFormat.format(Date(it.date)), it.url, it.enclosureUrl)
    }

    /**
     * Calculate numerical hash of provided string
     * */
    private fun stringHash31(string: String): Long {
        var h = 1125899906842597L // prime

        for (i in 0 until string.length) {
            h = 31*h + string[i].toLong()
        }
        return h
    }

    /**
     * Calls when user select entry
     * */
    override fun onItemSelected(item: EntryListItem) {
        view.navigateToEntryView(item.url)
    }
}
package com.alexgator.aurelia.presenter

import android.content.Context
import android.util.Log
import com.alexgator.aurelia.R
import com.alexgator.aurelia.model.DataManager
import com.alexgator.aurelia.view.EntryView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 15.03.18.
 */

interface EntryPresenter {
    fun onCreate(url: String)
    fun onOpenFullVersionBtnClick()
}

class EntryPresenterImpl(private val view: EntryView): EntryPresenter {

    companion object {
        val dateFormat = SimpleDateFormat("dd MMM 'в' HH:mm", Locale("ru"))
    }

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private var articleUrl: String = ""

    override fun onCreate(url: String) {
        view.component.inject(this)

        fetchAndShowEntry(url)
    }

    private fun fetchAndShowEntry(url: String) {
        launch {
            try {
                val article = dataManager.getArticle(url).await()

                articleUrl = article.url
                val date = dateFormat.format(Date(article.date))

                launch(UI) {
                    view.showTitle(article.title)
                    view.showDate(date)
                    view.showText(article.body)
                    view.showImage(article.imageUrl)
                }
            } catch (e: Exception) {
                launch(UI) {
                    view.showCriticalException(appContext.getString(R.string.data_fetch_failed))
                }
            }
        }
    }

    override fun onOpenFullVersionBtnClick() {
        view.openUrl(articleUrl)
    }
}
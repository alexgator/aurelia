package com.alexgator.aurelia

import android.app.Activity
import android.app.Application
import com.alexgator.aurelia.di.AppComponent
import com.alexgator.aurelia.di.AppModule
import com.alexgator.aurelia.di.DaggerAppComponent
import javax.inject.Inject

/**
 * Created by AlexGator on 15.03.18.
 */

val Activity.app: App
    get() = application as App

class App : Application() {

    /*
     * The code won't be executed until
     * [component.inject(this)]
     * is done, so that by that time [this] already exist.
     * */
    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }

}
package com.alexgator.aurelia.di

import com.alexgator.aurelia.presenter.EntryPresenterImpl
import com.alexgator.aurelia.view.EntryActivity
import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 15.03.18.
 */

@Scope annotation class EntryActivityScope

@EntryActivityScope
@Subcomponent(modules = [EntryModule::class])
interface EntryComponent {
    fun inject(activity: EntryActivity)
    fun inject(presenter: EntryPresenterImpl)
}
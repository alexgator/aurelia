package com.alexgator.aurelia.di

import android.content.Context
import com.alexgator.aurelia.App
import com.alexgator.aurelia.model.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by AlexGator on 15.03.18.
 */

@Module
class AppModule(private val app: App) {
    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideDataManager(): DataManager = DataManagerImpl(app)

    @Provides
    @Singleton
    fun provideRSSHelper(): RSSHelper = RSSHelperImpl()

    @Provides
    @Singleton
    fun provideAPIHelper(): APIHelper = APIHelperImpl()

    @Provides
    @Singleton
    fun provideDBHelper(context: Context): DBHelper = DBHelperImpl(context)

}
package com.alexgator.aurelia.di

import com.alexgator.aurelia.presenter.FeedPresenterImpl
import com.alexgator.aurelia.view.FeedActivity
import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 15.03.18.
 */

@Scope annotation class FeedActivityScope

@FeedActivityScope
@Subcomponent(modules = [FeedModule::class])
interface FeedComponent {
    fun inject(activity: FeedActivity)
    fun inject(presenter: FeedPresenterImpl)
}
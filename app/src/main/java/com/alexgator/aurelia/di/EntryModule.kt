package com.alexgator.aurelia.di

import com.alexgator.aurelia.presenter.EntryPresenter
import com.alexgator.aurelia.presenter.EntryPresenterImpl
import com.alexgator.aurelia.view.EntryActivity
import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 15.03.18.
 */

@Module
class EntryModule(private val activity: EntryActivity) {
    @Provides
    @EntryActivityScope
    fun providePresenter(): EntryPresenter = EntryPresenterImpl(activity)
}
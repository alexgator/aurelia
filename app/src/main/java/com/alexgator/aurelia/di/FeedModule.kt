package com.alexgator.aurelia.di

import com.alexgator.aurelia.presenter.FeedPresenter
import com.alexgator.aurelia.presenter.FeedPresenterImpl
import com.alexgator.aurelia.view.FeedActivity
import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 15.03.18.
 */

@Module
class FeedModule(private val activity: FeedActivity) {
    @Provides
    @FeedActivityScope
    fun providePresenter(): FeedPresenter = FeedPresenterImpl(activity)
}
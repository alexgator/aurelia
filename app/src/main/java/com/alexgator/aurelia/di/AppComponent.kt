package com.alexgator.aurelia.di

import android.app.Application
import com.alexgator.aurelia.model.DataManagerImpl
import dagger.Component
import javax.inject.Singleton

/**
 * Created by AlexGator on 15.03.18.
 */


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: Application)
    fun inject(dataManager: DataManagerImpl)

    fun plus(module: FeedModule): FeedComponent
    fun plus(module: EntryModule): EntryComponent
}